/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.impexscanner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import java.util.zip.GZIPInputStream;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Tools {

    public List<String> readSitemap(File file) {
        ArrayList<String> parts = new ArrayList<>();
        try {
            Scanner sc = new Scanner(file);
            sc.useDelimiter("\n");
            while (sc.hasNext()) {
                parts.add(sc.next());
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File not found.");
            ex.printStackTrace();
        }
        System.out.println("Sitemaps count: " + parts.size());
        parts.forEach(System.out::println);
        return parts;
    }

    public void print(List<String> list, String name) throws IOException {
        System.out.println("print result");
        PrintWriter out = new PrintWriter(".\\result\\"+name);
        list.forEach(line -> {
            out.println(line);
        });
        out.flush();
        out.close();
    }

    public void getArchives() throws IOException {
        Jsoup.parse(new File(".\\sitemapindex.xml"), "UTF-8").getElementsByTag("loc").forEach(e -> {
            try {
                URLConnection connection = new URL(e.text()).openConnection();
                String redirect = connection.getHeaderField("Location");
                if (redirect != null) {
                    connection = new URL(redirect).openConnection();
                }
                try (InputStream in = connection.getInputStream()) {
                    Files.copy(in, new File(".\\archives\\" + e.text().split("\\/")[4]).toPath(), StandardCopyOption.REPLACE_EXISTING);
                    System.out.println("Copy:" + e.text());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

    }

    public void extractArch() throws IOException {
        System.out.println(Files.list(new File(".\\archives\\").toPath()));
        Files.list(new File(".\\archives\\").toPath()).forEach(a -> {
            try {
                InputStream in = new GZIPInputStream(new FileInputStream(a.toFile()));
                Files.copy(in, new File(".\\sitemaps\\" + a.getFileName() + ".txt").toPath(), StandardCopyOption.REPLACE_EXISTING);
                System.out.println("Extract:" + a.getFileName());
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
    }

    public List<String> extractParts(File f) throws IOException {
        ArrayList<String> links = new ArrayList<>();
        System.out.println("Read:" + f.getName());
        try {
            Scanner sc = new Scanner(f.toPath());
            sc.useDelimiter("\n");
            while (sc.hasNext()) {
                links.add(sc.next().replace("http://www.impex-jp.com/zip/nomer/", "").replace(".html", ""));
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File not found.");
            ex.printStackTrace();
        }
        return links;
    }

}
