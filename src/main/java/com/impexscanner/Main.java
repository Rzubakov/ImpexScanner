package com.impexscanner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Main {

    public static void main(String... args) {
        Tools tools = new Tools();
        try {

            tools.getArchives();
            tools.extractArch();
            Files.list(new File(".\\sitemaps\\").toPath()).forEach(f -> {
                try {
                    tools.print(tools.extractParts(f.toFile()), f.getFileName().toString());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            });

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
